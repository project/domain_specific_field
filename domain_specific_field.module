<?php

/**
 * @file
 * Contains domain_specific_field.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function domain_specific_field_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the domain_specific_field module.
    case 'help.page.domain_specific_field':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This is for domain specific field') . '</p>';
      return $output;

    default:'';
  }
}

/**
 * Implements hook_node_insert().
 */
function domain_specific_field_node_insert(EntityInterface $node) {
  // Insert domian specific value  in table on node save.
  if ($node->bundle() == 'article') {
    $values = $node->toArray();
    if ($node->id() != NULL) {
      if ($values['field_domain_access'][0]['target_id'] != NULL) {
        for ($i = 0; $i < count($values['field_domain_access']); $i++) {
          $query = \Drupal::database()->insert('field_domain_body');
          $query->fields([
            'nodeID ',
            'bodyText',
            'domain',
          ]);
          $query->values([
            $node->id(),
            $values['field_domain_specific_body_text'][0]['value'],
            $values['field_domain_access'][$i]['target_id'],
          ]);
          $query->execute();
        }
      }
    }
  }
}

/**
 * Implements hook_node_update().
 */
function domain_specific_field_node_update(EntityInterface $node) {
  // Get current domain data.
  $data = \Drupal::service('domain.negotiator')->getActiveDomain()->toArray();

  // Update data if node id is in table.
  if ($node->bundle() == 'article') {
    $values = $node->toArray();
    if ($node->id() != NULL) {
      for ($i = 0; $i < count($values['field_domain_access']); $i++) {
        $query = \Drupal::database()->select('field_domain_body', 'nfd');
        $query->addField('nfd', 'nodeID');
        $query->condition('nfd.domain', $values['field_domain_access'][$i]['target_id']);
        $query->condition('nfd.nodeID', $node->id());
        $query->range(0, 1);
        $nid = $query->execute()->fetchField();
        if ($nid == TRUE) {
          $query = \Drupal::database()->update('field_domain_body');
          $query->fields([
            'bodyText' => $values['field_domain_specific_body_text'][0]['value'],
          ]);
          $query->condition('field_domain_body.domain', $data['id']);
          $query->condition('field_domain_body.nodeID', $node->id());
          $query->execute();
        }
        else {
          // Insert domian specific value  in table on node update.
          // if id is not in table.
          $query = \Drupal::database()->select('field_domain_body', 'nfd');
          $query->addField('nfd', 'nodeID');
          $query->condition('nfd.domain', $values['field_domain_access'][$i]['target_id']);
          $query->condition('nfd.nodeID', $node->id());
          $query->range(0, 1);
          $nid = $query->execute()->fetchField();
          if ($nid == FALSE) {
            $query = \Drupal::database()->insert('field_domain_body');
            $query->fields([
              'nodeID ',
              'bodyText',
              'domain',
            ]);
            $query->values([
              $node->id(),
              $values['field_domain_specific_body_text'][0]['value'],
              $values['field_domain_access'][$i]['target_id'],
            ]);
            $query->execute();
          }
        }
      }
    }
  }
}

/**
 * Implements hook_node_delete().
 *
 * Delete domain specific body text value on node delete.
 */
function domain_specific_field_node_delete(EntityInterface $node) {
  $query = \Drupal::database()->delete('field_domain_body');
  $query->condition('field_domain_body.nodeID', $node->id());
  $query->execute();
}

/**
 * Implements hook_form_alter().
 *
 * Display domain specific body text value on node edit.
 */
function domain_specific_field_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $node = \Drupal::routeMatch()->getParameter('node');
  if (!empty($node) && is_object($node)) {
    $all_domain = $node->toArray();
    if ($all_domain['field_domain_all_affiliates'][0]['value'] == '0') {
      $type = $node->bundle();
      $data = \Drupal::service('domain.negotiator')->getActiveDomain()->toArray();
      if ($type == 'article') {
        $query = \Drupal::database()->select('field_domain_body', 'nfd');
        $query->addField('nfd', 'bodyText');
        $query->condition('nfd.domain', $data['id']);
        $query->condition('nfd.nodeID', $node->id());
        $query->range(0, 1);
        $domain_body = $query->execute()->fetchField();
        $form['field_domain_specific_body_text']['widget']['0']['#default_value'] = $domain_body;
        $form['#validate'][] = 'domain_specific_field_delete_removed_data';
      }
    }
  }

}

/**
 * Implements hook_entity_presave().
 *
 * Update data according domain access array data.
 */
function domain_specific_field_delete_removed_data($form, FormStateInterface $form_state) {
  $node = \Drupal::routeMatch()->getParameter('node');
  if (!empty($node) && is_object($node)) {
    $domain_available = [];
    $type = $node->bundle();
    if ($type == 'article') {
      $domain_data = $form_state->getValues();
      for ($i = 0; $i < count($domain_data['field_domain_access']); $i++) {
        $domain_available[] = $domain_data['field_domain_access'][$i]['target_id'];
      }
      $query = \Drupal::database()->select('field_domain_body', 'nfd');
      $query->addField('nfd', 'domain');
      $query->condition('nfd.nodeID', $node->id());
      $domain_exist = $query->execute()->fetchAll();
      if (!empty($domain_available) && !empty($domain_exist)) {
        foreach ($domain_exist as $data) {
          if (!in_array($data->domain, $domain_available)) {
            $query = \Drupal::database()->delete('field_domain_body');
            $query->condition('field_domain_body.nodeID', $node->id());
            $query->condition('field_domain_body.domain', $data->domain);
            $query->execute();
          }
        }
      }
      return;
    }
  }
}

/**
 * Implements hook_entity_display_build_alter().
 *
 * Create field_domain_specific_body_text markup in build array.
 */
function domain_specific_field_entity_display_build_alter(&$output, $context) {
  $entity = $context['entity'];
  $nid = $entity->id();
  $entity_data = $entity->toArray();
  if (isset($entity_data['field_domain_all_affiliates']) && $entity_data['field_domain_all_affiliates'][0]['value'] == '0') {
    $data = \Drupal::service('domain.negotiator')->getActiveDomain()->toArray();
    $query = \Drupal::database()->select('field_domain_body', 'nfd');
    $query->addField('nfd', 'bodyText');
    $query->condition('nfd.domain', $data['id']);
    $query->condition('nfd.nodeID', $nid);
    $query->range(0, 1);
    $domain_body = $query->execute()->fetchField();
    $display_options = $context['display']->getComponent('field_domain_specific_body_text');
    if (isset($display_options['settings']['alter'])) {
      $output['field_domain_specific_body_text'][] = ['#markup' => 'domain_specific_field_entity_display_build_alter'];
    }
    if (isset($output['field_domain_specific_body_text'])) {
      $output['field_domain_specific_body_text'] = ['#markup' => $domain_body];
    }
  }
}
